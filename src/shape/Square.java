package shape;

/**
 *
 * @author rushil
 */
public class Square extends Shape {
    
    public Square(double side) {
        
    }
    @Override
    public double getArea(double side) {
        return side*side;
    }

    @Override
    public double getPerimeter(double side) {
        return 4*side;
    }
    
}
