package shape;

/**
 *
 * @author rushil
 */
public abstract class Shape {
    public abstract double getArea(double value);
    public abstract double getPerimeter(double value);
    /**
     * @param args the command line arguments
     */
    
}
