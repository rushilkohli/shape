package shape;

/**
 *
 * @author rushil
 */
public class ShapesSimulation {
    public static void main(String[] args) {
        Circle circle = new Circle(8);
        Square square = new Square(6);

        System.out.println("The Area of Circle is: " + circle.getArea(8));
        System.out.println("The Perimeter of Circle is: " + circle.getPerimeter(8));
        System.out.println("The Area of Square is: " + square.getArea(6));
        System.out.println("The Perimeter of Square is: " + square.getPerimeter(6));
    }
}
