package shape;

/**
 *
 * @author rushil
 */


public  class Circle extends Shape {

    public Circle(double radius) {
        
    }
    @Override
    public double getArea(double radius){
        return Math.PI*Math.pow(radius, 2);
    }

    @Override
    public double getPerimeter(double radius) {
        return 2*Math.PI*radius; 
    }
}
